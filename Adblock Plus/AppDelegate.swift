/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa
import SafariServices
import StoreKit

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    private let defaults = UserDefaults()
    var hasRun = UserDefaults(suiteName: Constants.groupIdentifier)?.bool(forKey: Constants.hasRun) ?? false

    func applicationDidFinishLaunching(_ notification: Notification) {
        if hasRun == false {
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(onboardingDidDismissNotificationReceived(_:)),
                                                   name: Notification.Name(rawValue: "onboardingDidDismiss"),
                                                   object: nil)
        }
    }

    func applicationShouldTerminateAfterLastWindowClosed (_ theApplication: NSApplication) -> Bool {
        return true
    }

    func applicationDidBecomeActive(_ notification: Notification) {
        if hasRun == true {
            promptForReview()
        }
    }

    func applicationWillTerminate(_ notification: Notification) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "onboardingDidDismiss"), object: nil)
    }

@objc
    func onboardingDidDismissNotificationReceived(_ notification: Notification) {
        promptForReview()
    }

    private func promptForReview() {
        let lastReviewedVersion = defaults.string(forKey: Constants.lastReviewedVersion_key)
        let currentVersion = Bundle.main.appVersionBuildCombined
        if lastReviewedVersion != currentVersion {
            SFContentBlockerManager.getStateOfContentBlocker(withIdentifier: Constants.contentBlockerIdentifier) { state, _ in
                if state?.isEnabled == true {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "showReviewPrompt"), object: nil)
                }
            }
        }
    }
}
