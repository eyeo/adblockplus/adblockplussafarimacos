/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa
import SafariServices

class GeneralViewController: NSViewController, NSMenuDelegate {

    @IBOutlet weak var generalTitle: NSTextFieldCell!
    @IBOutlet weak var languagePopUpButton: NSPopUpButton!
    @IBOutlet weak var readMoreButton: NSButton!
    @IBOutlet weak var acceptableAdsCheckbox: NSButton!
    @IBOutlet weak var acceptableAdsSpinner: NSProgressIndicator!

    /// Shared UserDefaults
    private let groupDefaults = UserDefaults(suiteName: Constants.groupIdentifier)
    private let filterlistManager = FilterlistManager()

    /// Notifications
    private let onboardingDismissalNotification = Notification.Name(rawValue: "onboardingDidDismiss")

    /// Opens URL when button tapped
    ///
    /// - Parameter button: Unused
    @IBAction func openAcceptableAdsReadMoreURL(_ button: NSButton) {
        if let url = URL(string: Constants.acceptableAdsReadMoreURL) {
            NSWorkspace.shared.open(url)
        }
    }

    /// When checkbox is toggled this saves the state in NSUserDefaults
    ///
    /// - Parameter button: The button that was interacted with.
    @IBAction func toggleAcceptableAds(_ button: NSButton) {
        let checkboxState = button.state.rawValue

        switch checkboxState {
        // State is disabled
        case 0:
            groupDefaults?.set(false, forKey: Constants.acceptableAdsEnabled)

        // State is enabled
        case 1:
            groupDefaults?.set(true, forKey: Constants.acceptableAdsEnabled)
        // State is mixed, however this should not occur due to option being disabled.
        // Should this error occur, reset to first run default.
        default:
            button.state = .on
            groupDefaults?.set(false, forKey: Constants.acceptableAdsEnabled)
        }
        reloadContentBlocker()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        styleReadMoreButton(button: readMoreButton)
        setAcceptableAdsCheckboxState(button: acceptableAdsCheckbox)
        populateLanguagePopUpButton()
        // Added a short delay to reload content blocker from when app launches to ensure that
        // any updates (such as copying filter lists and applying allowlists don't clash.
        // I haven't had this problem during my testing however, but may need a little extra time
        // for slower machines. In future, this can be refactored into a more elegant solution.
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            self.reloadContentBlocker()
        })
    }

    override func viewWillAppear() {
        super.viewWillAppear()
        generalTitle.title = "General".localized
        setPopupButtonSelectedItem()
    }

    /// When app is launched, this method checks NSUserDefaults and sets the checkbox as per user preference.
    ///
    /// - Parameter button: The button to update state of.
    private func setAcceptableAdsCheckboxState(button: NSButton) {
        guard let acceptableAdsEnabled = groupDefaults?.object(forKey: Constants.acceptableAdsEnabled) as? Bool else {
            groupDefaults?.set(true, forKey: Constants.acceptableAdsEnabled)
            button.state = .on
            return
        }
        switch acceptableAdsEnabled {
        case true:
            button.state = .on
        case false:
            button.state = .off
        }
    }

    /// Applies colour & font style to NSButton Text
    ///
    /// - Parameter button: The button to update style of.
    private func styleReadMoreButton(button: NSButton) {
        let readMoreString = button.title
        let textColor: NSColor
        if #available(OSX 10.14, *) {
            textColor = NSColor.controlAccentColor
        } else {
            textColor = NSColor.systemBlue
        }
        let readMoreAttributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.foregroundColor: textColor,
            NSAttributedString.Key.font: NSFont.systemFont(ofSize: 10)
        ]
        button.attributedTitle = NSAttributedString(string: readMoreString, attributes: readMoreAttributes)
    }

    /// Changes enabled state of checkbox, popup button and adds an animation spinner.
    /// This should be used while loading blocklists. Such as when acceptable ads button is toggled.
    /// Always called on main thread.
    ///
    /// - enabled: If passed in true, this will enable the UI element. False, will disable them.
    private func changeAcceptableAdsCheckboxState(enabled: Bool) {
        DispatchQueue.main.async {
            switch enabled {
            case true:
                self.acceptableAdsCheckbox.isEnabled = true
                self.languagePopUpButton.isEnabled = true
                self.acceptableAdsSpinner.stopAnimation(nil)
            case false:
                self.acceptableAdsCheckbox.isEnabled = false
                self.languagePopUpButton.isEnabled = false
                self.acceptableAdsSpinner.startAnimation(nil)
            }
        }
    }

    /// Checks if content blocker extension is enabled, and then calls upon it to reload.
    /// Also updates UI in this view controller to avoid use while processing.
    private func reloadContentBlocker() {
        SFContentBlockerManager.getStateOfContentBlocker(withIdentifier: Constants.contentBlockerIdentifier) { state, _ in
            let uwState = state?.isEnabled ?? false
            if uwState {
                self.changeAcceptableAdsCheckboxState(enabled: false)
                SFContentBlockerManager.reloadContentBlocker(withIdentifier: Constants.contentBlockerIdentifier, completionHandler: { _ in
                    self.changeAcceptableAdsCheckboxState(enabled: true)
                })
            }
        }
    }

    /// Sets the language popup button to the current user setting.
    private func setPopupButtonSelectedItem() {
        DispatchQueue.main.async {
            if let selectedFilterlist = self.groupDefaults?.string(forKey: Constants.selectedFilterList) {
                let filterlist = self.filterlistManager.getFilterlistWithLanguage(FilterlistLanguage(rawValue: selectedFilterlist) ?? .english)
                let selectedItemTag = filterlist.tag
                for item in self.languagePopUpButton.itemArray where item.tag == selectedItemTag {
                    self.languagePopUpButton.selectItem(withTag: item.tag)
                    break
                }
            }
        }
    }

    /// Populates the language popup button with text.
    private func populateLanguagePopUpButton() {
        for (index, filterlist) in Filterlists.filterlists.enumerated() {
            languagePopUpButton.addItem(withTitle: filterlist.title)
            let menuItemAttributedString = NSMutableAttributedString()
                .normal("\(filterlist.title) ")
                .small(filterlist.description)
            let menuItem = languagePopUpButton.item(at: index)
            menuItem?.attributedTitle = menuItemAttributedString
            menuItem?.tag = filterlist.tag
        }
    }

    // When a language menu item is selected, select the filter list
    // and store the result in UserDefaults, before reloading.
    @IBAction func didSelectMenuEntry(_ sender: NSPopUpButton) {
        guard sender == languagePopUpButton else { return }
        let selectedItem = sender.selectedItem
        guard let uwTag = selectedItem?.tag else { return }
        let filterlists = Filterlists.filterlists
        let newActiveFilterlist = filterlists.first(where: { $0.tag == uwTag })
        let newActiveFilterlistLanguage = newActiveFilterlist?.language.rawValue
        groupDefaults?.set(newActiveFilterlistLanguage, forKey: Constants.selectedFilterList)
        filterlistManager.copyFilterlistsToSharedContainer()
        reloadContentBlocker()
    }
}
