/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa
import SafariServices
import StoreKit

class HomeViewController: NSViewController {

    @IBOutlet weak var generalContainerView: NSView!
    @IBOutlet weak var allowlistContainerView: NSView!
    @IBOutlet weak var helpContainerView: NSView!
    private var contentBlockerIsEnabled = false
    private var toolbarIconIsEnabled = false
    private let sidebarItemDataSource = SidebarItemDataSource()
    private let defaults = UserDefaults()
    private let groupDefaults = UserDefaults(suiteName: Constants.groupIdentifier)
    private let filterlistManager = FilterlistManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(showReviewPromptNotificationReceived(_:)),
                                               name: Notification.Name(rawValue: "showReviewPrompt"),
                                               object: nil)
        let hasRun = groupDefaults?.bool(forKey: Constants.hasRun)
        let installedAppVersion = groupDefaults?.string(forKey: Constants.storedAppVersion)
        let currentVersionNumber = Constants.currentVersionNumber

        // If hasRun == false, Onboarding logic will be called & Filter lists will be copied to shared container.
        // If Installed App Version != Current Version, local Filter lists will be copied to shared container.
        if hasRun == false {
            let showOnboarding = true
            groupDefaults?.set(showOnboarding, forKey: Constants.showOnboarding)
            groupDefaults?.set(currentVersionNumber, forKey: Constants.storedAppVersion)
            groupDefaults?.set(true, forKey: Constants.hasRun)
            filterlistManager.copyFilterlistsToSharedContainer()
        } else if installedAppVersion != currentVersionNumber {
            groupDefaults?.set(currentVersionNumber, forKey: Constants.storedAppVersion)
            // copy new filter lists
            filterlistManager.copyFilterlistsToSharedContainer()
            // Apply Allowlist
            let allowlistItemDataSource = AllowlistItemDataSource()
            let allowlistArray = allowlistItemDataSource.getAllowlistArray()
            if allowlistArray.isEmpty == false {
                let allowlistManager = AllowlistManager()
                allowlistManager.applyAllowlist(allowlistArray)
            }
        }
    }

    override func viewDidAppear() {
        SFContentBlockerManager.getStateOfContentBlocker(withIdentifier: Constants.contentBlockerIdentifier) { state, _ in
            self.contentBlockerIsEnabled = state?.isEnabled ?? false
            SFSafariExtensionManager.getStateOfSafariExtension(withIdentifier: Constants.safariToolbarIdentifier) { state, _ in
                self.toolbarIconIsEnabled = state?.isEnabled ?? false
                self.checkOnboardingTriggers()
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "showReviewPrompt"), object: nil)
    }

    /// Checks if content blocker is disabled or if showOnboarding is set to true.
    /// Either of these states will trigger the onboarding flow to start.
    /// Will return early if both extensions (content blocker & toolbar) are enabled.
    func checkOnboardingTriggers() {
        let showOnboarding = groupDefaults?.bool(forKey: Constants.showOnboarding)

        // If both extensions are enabled, set first run onboarding not to show and fallthrough.
        // else continue with onboarding logic
        if contentBlockerIsEnabled && toolbarIconIsEnabled {
            groupDefaults?.set(false, forKey: Constants.showOnboarding)
        } else {
                if contentBlockerIsEnabled == false || showOnboarding == true {
                    DispatchQueue.main.async {
                        self.groupDefaults?.set(false, forKey: Constants.showOnboarding)
                        self.showOnboardingFlow()
                    }
                }
        }
    }

    func showOnboardingFlow() {
        if let onboardingViewController = self.storyboard?.instantiateController(withIdentifier: Constants.OnboardingVC) as? NSViewController {
            presentAsModalWindow(onboardingViewController)
        }
    }

@objc
    func showReviewPromptNotificationReceived(_ notification: Notification) {
        let currentVersion = Bundle.main.appVersionBuildCombined
        self.defaults.set(currentVersion, forKey: Constants.lastReviewedVersion_key)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: { SKStoreReviewController.requestReview() })
    }
}

// MARK: - Extends ViewController to act as data source for the sidebar NSCollectionView
extension HomeViewController: NSCollectionViewDataSource {

    static let sidebarItem = "SidebarItem"

    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        let numberOfItems = sidebarItemDataSource.numberOfSidebarItems()
        return numberOfItems
    }

    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        let item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: HomeViewController.sidebarItem), for: indexPath)
        item.textField?.stringValue = sidebarItemDataSource.sidebarTitleForIndexPath(indexPath: indexPath)
        item.imageView?.image = sidebarItemDataSource.sidebarIconForIndexPath(indexPath: indexPath)
        item.isSelected = false // prevent highlight of General + Allowlist on macOS 10.12

        // pre-selects first item on initial load.
        if indexPath == [0, 0] {
            collectionView.selectItems(at: [indexPath], scrollPosition: .top)
        }

        return item
    }
}

// MARK: - Extends ViewController to act as delegate for the sidebar NSCollectionView
extension HomeViewController: NSCollectionViewDelegate {
    // A rudimentary paging manager. Once a sidebar item is selected,
    // this function will determine which item and change container view
    // visibility within the main view.
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        let index = indexPaths.first?.dropFirst()[0] ?? 0
        switch index {
        case 0:
            // General Tab Clicked
            generalContainerView.isHidden = false
            allowlistContainerView.isHidden = true
            helpContainerView.isHidden = true
        case 1:
            // Allowlist Tab Clicked
            generalContainerView.isHidden = true
            allowlistContainerView.isHidden = false
            helpContainerView.isHidden = true
        case 2:
            // Help Tab Clicked
            generalContainerView.isHidden = true
            allowlistContainerView.isHidden = true
            helpContainerView.isHidden = false
        default:
            break
        }
    }
}
