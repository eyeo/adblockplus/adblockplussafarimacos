/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa

class SidebarItem: NSCollectionViewItem {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.wantsLayer = true
    }

    /// When sidebar item is highlighted, add black (with 10% alpha) as background colour.
    override var isSelected: Bool {
        didSet {
            view.layer?.backgroundColor = isSelected ? NSColor.customColor(.sidebarItemSelected).cgColor : nil
        }
    }

    override func viewDidAppear() {
        // Highlights first item on initial load.
        if imageView?.image == #imageLiteral(resourceName: "icon-general") {
            isSelected = true
        }
    }

    func setHighlight(selected: Bool) {
        view.layer?.backgroundColor = isSelected ? NSColor.customColor(.sidebarItemSelected).cgColor : nil
    }
}
