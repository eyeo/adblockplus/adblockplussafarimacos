/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa

/// Datasource for allowlist NSCollectionView
class AllowlistItemDataSource {

    /// Checks NSUserDefaults for URL's added to the allowlist.
    ///
    /// - Returns: Either an Array with URL Strings or an empty array if no data found.
    func getAllowlistArray() -> [String] {
        let container = Constants.groupIdentifier
        let groupDefaults = UserDefaults(suiteName: container)
        let allowlistArray = groupDefaults?.stringArray(forKey: Constants.allowlistArray) ?? [String]()
        return allowlistArray
    }

    func appendToAllowlist(hostname: String) {
        var allowlistArray = self.getAllowlistArray()
        allowlistArray.append(hostname)
        let container = Constants.groupIdentifier
        let groupDefaults = UserDefaults(suiteName: container)
        groupDefaults?.set(allowlistArray, forKey: Constants.allowlistArray)
    }

    func allowlistItemForIndexPath(indexPath: IndexPath) -> String {
        let array = getAllowlistArray()
        return array[indexPath.item]
    }

    func removeURLfromArray(url: String) {
        var allowlistArray = self.getAllowlistArray()
        if let index = allowlistArray.firstIndex(of: url) {
            allowlistArray.remove(at: index)
            let container = Constants.groupIdentifier
            let groupDefaults = UserDefaults(suiteName: container)
            groupDefaults?.set(allowlistArray, forKey: Constants.allowlistArray)
        }
    }
    /// - Returns: Total number of websites on the allowlist.
    func numberOfAllowlistItems() -> Int {
        let allowlistCount = getAllowlistArray().count
        return allowlistCount
    }
}
