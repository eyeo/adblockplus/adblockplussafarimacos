/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

struct Constants {
    // App Bundle Identifiers
    static let appIdentifier = "org.adblockplus.adblockplussafarimac"
    static let contentBlockerIdentifier = "org.adblockplus.adblockplussafarimac.AdblockPlusSafariContentBlocker"
    static let safariToolbarIdentifier = "org.adblockplus.adblockplussafarimac.AdblockPlusSafariToolbar"
    static let groupIdentifier = "GRYYZR985A.org.adblockplus.adblockplussafarimac"

    // Shared Filterlist Filepaths
    static let sharedFilterlist = "sharedFilterlist"
    static let sharedFilterlistOriginal = "sharedFilterlist_original"
    static let sharedFilterlistWithExceptions = "sharedFilterlist+exceptionrules"
    static let sharedFilterlistWithExceptionsOriginal = "sharedFilterlist+exceptionrules_original"

    // User Defaults Keys
    /// Returns a Bool if app has previously run
    static let hasRun = "hasRun"
    /// Returns a string of the stored app version, set when app last run.
    static let storedAppVersion = "installedAppVersion"
    /// Returns a Bool to see if onboarding should be shown
    static let showOnboarding = "showOnboarding"
    /// Returns a Bool to see if Acceptable Ads is enabled
    static let acceptableAdsEnabled = "acceptableAdsEnabled"
    /// Returns an Array(String) of domains to be added to the allowlist
    static let allowlistArray = "whitelistArray"
    /// Returns the selected filter list value
    static let selectedFilterList = "selectedFilterList"
    // Returns string representing last reviewed app version
    // swiftlint:disable:next identifier_name
    static let lastReviewedVersion_key = "lastReviewedVersion_key"

    // URLs
    static let acceptableAdsReadMoreURL = "https://adblockplus.org/en/acceptable-ads#criteria"
    static let helpdeskURL = "https://help.adblockplus.org/"
    static let supportEmailURL = "mailto:support@adblockplus.org"
    static let facebookURL = "https://adblockplus.org/redirect?link=social_facebook"
    static let twitterURL = "https://adblockplus.org/redirect?link=social_twitter"

    // Storyboard Identifiers
    static let OnboardingVC = "OnboardingViewController"

    // NSCollectionViewItems
    static let sidebarItem = "SidebarItem"
    static let allowlistItem = "AllowlistItem"

    // App Version Number
    /// Returns a string of the version & build of the App from info.plist
    static var currentVersionNumber: String? {
        guard
            let appVersionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String,
            let appBuildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String
            else {
                return "info.plist Read Error"
        }
        let versionBuildCombined = "\(appVersionNumber) (\(appBuildNumber))"
        return versionBuildCombined
    }

    // Unit Testing Check
    /// Returns a bool to determine unit tests are being run.
    static var isRunningTests: Bool {
        let env: [String: String] = ProcessInfo.processInfo.environment
        return env["XCInjectBundleInto"] != nil
    }
}
