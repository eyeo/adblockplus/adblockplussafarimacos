/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa

/// DataSource for sidebar NSCollectionView
class SidebarItemDataSource {

    let sidebarItemList = ["General".localized,
                           "Allowlist".localized,
                           "Help".localized]
    let sidebarItemIcons = [#imageLiteral(resourceName: "icon-general"), #imageLiteral(resourceName: "icon-allowlist"), #imageLiteral(resourceName: "icon-help")]

    /// - Returns: total of items in sidebarItemList
    func numberOfSidebarItems() -> Int {
        return sidebarItemList.count
    }

    /// - Returns: title for sidebarItem
    func sidebarTitleForIndexPath(indexPath: IndexPath) -> String {
        return sidebarItemList[indexPath.item]
    }

    /// - Returns: icon for sidebarItem
    func sidebarIconForIndexPath(indexPath: IndexPath) -> NSImage {
        return sidebarItemIcons[indexPath.item]
    }
}
