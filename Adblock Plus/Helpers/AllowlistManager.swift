/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa
import SafariServices

class AllowlistManager {

    /// Uses input array and adds to Content Blocker JSON Files.
    ///
    /// - Parameter domainList: An array of URL's to be added to the allowlist.
    func applyAllowlist(_ domainList: [String]) {
        let allowlistArray = domainList.compactMap { ["action": ["type": "ignore-previous-rules"] as [String: Any],
                                                      "trigger": ["url-filter": ".*", "if-top-url": [$0]]]
        }

        guard let sharedContainer = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: Constants.groupIdentifier) else { return }
        let easylistOriginalPath = sharedContainer.appendingPathComponent("\(Constants.sharedFilterlistOriginal).json")
        let easylistExceptionsOriginalPath = sharedContainer.appendingPathComponent("\(Constants.sharedFilterlistWithExceptionsOriginal).json")

        guard let easylistData = try? Data(contentsOf: easylistOriginalPath),
              let easylistExceptionsData = try? Data(contentsOf: easylistExceptionsOriginalPath),
              let easylistJSON = (try? JSONSerialization.jsonObject(with: easylistData, options: [])) as? [[String: Any]],
              let easylistExceptionsJSON = (try? JSONSerialization.jsonObject(with: easylistExceptionsData,
                                                                              options: [])) as? [[String: Any]] else { return }

        let updatedEasylistJSON = easylistJSON + allowlistArray
        let updatedEasylistExceptionsJSON = easylistExceptionsJSON + allowlistArray

        let easylistJSONData = try? JSONSerialization.data(withJSONObject: updatedEasylistJSON, options: [.prettyPrinted])
        let easylistExceptionsJSONData = try? JSONSerialization.data(withJSONObject: updatedEasylistExceptionsJSON, options: [.prettyPrinted])

        let easylistPath = sharedContainer.appendingPathComponent("\(Constants.sharedFilterlist).json")
        let easylistExceptionsPath = sharedContainer.appendingPathComponent("\(Constants.sharedFilterlistWithExceptions).json")

        try? FileManager.default.removeItem(at: easylistPath)
        try? easylistJSONData?.write(to: easylistPath)

        try? FileManager.default.removeItem(at: easylistExceptionsPath)
        try? easylistExceptionsJSONData?.write(to: easylistExceptionsPath)
    }

    private func nsdataToJSON(_ data: Data) -> AnyObject {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as AnyObject
        } catch _ {
            print("Unable to serialize JSON")
        }
        return 0 as AnyObject
    }
}
