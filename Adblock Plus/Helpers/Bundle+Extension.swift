/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import AppKit

extension Bundle {
    /// App Build number as a string. Eg "42"
    var appBuild: String { getInfo("CFBundleVersion") }
    /// App Version number as a string. Eg "2.0"
    var appVersion: String { getInfo("CFBundleShortVersionString") }
    /// App Version & Build number combined as a string. Eg "2.0 (42)"
    var appVersionBuildCombined: String { "\(Bundle.main.appVersion) (\(Bundle.main.appBuild))" }

    fileprivate func getInfo(_ str: String) -> String { infoDictionary?[str] as? String ?? "x" }
}
