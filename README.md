# Adblock Plus for Safari (macOS)

Adblock Plus is a content blocker for Safari which blocks adverts from the web.
You can [download Adblock Plus](https://itunes.apple.com/us/app/adblock-plus-for-safari/id1432731683) for free on the Mac App Store.

## Building

### Requirements

- [Xcode 14](https://developer.apple.com/xcode/)
- [SwiftLint](https://github.com/realm/SwiftLint) (optional)
- [Carthage](https://github.com/Carthage/Carthage) (optional)

### Building in Xcode
*NOTE*: If you do not wish to make use of Fabric/Crashlytics, remove the references from the project before building and skip to step 3 of build instructions.

1. Copy the file `ABP-macOS-Secret-API-Env-Vars.sh` (available internally, sample file here (https://gitlab.com/eyeo/sandbox/adblockplussafarimacos/snippets/1781796)) into the same directory as `Adblock Plus for Safari.xcodeproj`.
2. Run `carthage update` to install additional Swift dependencies.
3. Open `Adblock Plus for Safari.xcodeproj` in Xcode.
4. Build & Run the project.